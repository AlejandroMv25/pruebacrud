<%-- 
    Document   : List
    Created on : 7/07/2019, 04:19:17 PM
    Author     : USUARIO
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="Model.Cliente"%>
<%@page import="ModelADO.ClienteDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div >
        <h1>Clientes</h1>
        <a href="Controlador?accion=add"> Agregar nuevo  </a> 
        <br>
        <br>
        
        <form method="GET" action="datos.jsp">

            <table >
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>TIPO DE DOCUMENTO</th>
                        <th>DOCUMENTO</th>
                        <th>NOMBRE</th>
                        <th>APELLIDO</th>
                        <th>ESTADO</th>
                        <th>NUMERO CELULAR</th>
                    </tr>
                </thead>
                <%
                    ClienteDAO dao= new ClienteDAO();
                    List<Cliente>list=dao.listar();
                    Iterator<Cliente>iter=list.iterator();
                    Cliente cli = null;
                    while(iter.hasNext()){
                        cli=iter.next();
                    }
                %>
                <tbody>
                    <tr>
                        <td><%= cli.getId() %></td>
                        <td><%= cli.getTipodocumento()%></td>
                        <td><%= cli.getDocumento()%></td>
                        <td><%= cli.getNombre()%></td>
                        <td><%= cli.getApellido()%></td>
                        <td><%= cli.getCelular()%></td>
                        <td>
                            <a  href="Controlador?accion=editar&id=<%= cli.getId() %>"> Editar  </a>
                            <a  href="Controlador?accion=eliminar&id=<%= cli.getId() %>"> Eliminar </a>
                        </td>
                    </tr>
                </tbody>
            </table> 
                        </div>
    </body>
</html>
