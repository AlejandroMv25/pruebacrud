<%-- 
    Document   : Edit
    Created on : 7/07/2019, 04:19:29 PM
    Author     : USUARIO
--%>

<%@page import="Model.Cliente"%>
<%@page import="ModelADO.ClienteDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
       
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% 
          ClienteDAO dao= new ClienteDAO();
          int id = Integer.parseInt((String)request.getAttribute("idcli"));
          Cliente c= (Cliente)dao.list(id);
        %>
        
           <h1>Editar Cliente!</h1>
        <form >
            
            Tipo de Documento: <br>
            <input type="text" name="txtTipoDoc" value="<%= c.getTipodocumento()%>"><br>
            Documento: <br>
            <input type="number" name="txtDoc" value="<%= c.getDocumento()%>"><br>
            Nombres: <br>
            <input type="text" name="txtNombre" value="<%= c.getNombre()%>"><br>
            Apellidos: <br>
            <input type="text" name="txtApellido" value="<%= c.getApellido()%>"><br>
            Numero celular: <br>
            <input type="number" name="txtCelular" value="<%= c.getCelular()%>"><br>
             <input type="hidden" name="txtid" value="<%= c.getId() %>">><br>
            <input  type="submit" name="accion" value="Actualizar"><br>
            
            
        </form>
    </body>
</html>
