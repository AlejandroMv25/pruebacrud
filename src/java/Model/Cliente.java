
package Model;

public class Cliente {
    int id;
    int documento;
    String tipodocumento;
    String nombre;
    String apellido;
    boolean estado;
    int celular;

    public Cliente() {
    }

    public Cliente(int documento, String tipodocumento, String nombre, String apellido, boolean estado, int celular) {
        this.documento = documento;
        this.tipodocumento = tipodocumento;
        this.nombre = nombre;
        this.apellido = apellido;
        this.estado = estado;
        this.celular = celular;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDocumento() {
        return documento;
    }

    public void setDocumento(int documento) {
        this.documento = documento;
    }

    public String getTipodocumento() {
        return tipodocumento;
    }

    public void setTipodocumento(String tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public int getCelular() {
        return celular;
    }

    public void setCelular(int celular) {
        this.celular = celular;
    }
    

}
