
package ModelADO;

import Config.Conexion;
import Interfaces.CRUD;
import Model.Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;
import java.util.Calendar;

public class ClienteDAO implements CRUD {
       Conexion cn = new Conexion();
       Connection con;
       PreparedStatement ps;
ResultSet rs;
       Cliente c = new Cliente();
    
    @Override
    public List listar() {
        ArrayList<Cliente>list= new ArrayList<>();
        String sql ="select * from cliente";
        try{
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs =  ps.executeQuery();
            while((rs.next())){
                Cliente cli = new Cliente();
                cli.setId(rs.getInt("Id"));
                cli.setTipodocumento(rs.getString("Tipo de documento"));
                cli.setDocumento(rs.getInt("Documento"));
                cli.setNombre(rs.getString("Nombre"));
                cli.setApellido(rs.getString("Apellido"));
                cli.setEstado(rs.getBoolean("Estado"));
                cli.setCelular(rs.getInt("Numero Celular"));
                list.add(cli);
            }
        }catch (Exception e){
            
        }
        return list;
    }

    @Override
    public Cliente list(int id) {

        String sql ="select * from cliente where Id="+id;
        try{
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs =  ps.executeQuery();
            while((rs.next())){
             
                c.setId(rs.getInt("Id"));
                c.setTipodocumento(rs.getString("Tipo de documento"));
                c.setDocumento(rs.getInt("Documento"));
                c.setNombre(rs.getString("Nombre"));
                c.setApellido(rs.getString("Apellido"));
                c.setEstado(rs.getBoolean("Estado"));
                c.setCelular(rs.getInt("Numero Celular"));
               
            }
        }catch (Exception e){
            
        }
        return c;
    }

    @Override
    public boolean add(Cliente cli) {
 String sql= "insert into cliente(Tipo de documento,Documento,Nombre,Apellido,Fecha registro,Estado,Numero Celular)"
         + "values('"+cli.getTipodocumento()+"','"+cli.getDocumento()+"','"+cli.getNombre()+"','"+cli.getApellido()+"','"+Calendar.getInstance()+"','"+1+"','"+cli.getCelular()+"')";
         
 try{
     con = cn.getConnection();
     ps= con.prepareStatement(sql);
     ps.executeUpdate();
 }catch( Exception e){
     
 }
 return false;
    }

    @Override
    public boolean edit(Cliente cli) {
        String sql= "update cliente set Tipo de documento= '"+cli.getTipodocumento()+"',Documento= '"+cli.getDocumento()+"',Nombre= '"+cli.getNombre()+"',Apellido = '"+cli.getApellido()+"',Numero Celular='"+cli.getCelular()+"' where Id="+cli.getId();
try{
    con= cn.getConnection();
    ps= con.prepareStatement(sql);
    ps.executeUpdate();
}catch (Exception e){
    
}
        return false; 
    }

    @Override
    public boolean delete(int id) {
        String sql = "delete from cliente where id="+id;
        try{
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        }catch (Exception e){
            
        }
        return false;
    }
    
}
