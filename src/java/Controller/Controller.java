/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Cliente;
import ModelADO.ClienteDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author USUARIO
 */
public class Controller extends HttpServlet {

    
    String list = "View/List.jsp";
    String add = "View/Add.jsp";
    String edit = "View/Edit.jsp";
    Cliente c = new Cliente();
    ClienteDAO dao = new ClienteDAO();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String acceso="";
        String action =request.getParameter("accion");
       if(action.equalsIgnoreCase("List")) {
           acceso= list;
           
       }else if(action.equalsIgnoreCase("add")){
           acceso=add;
       }else if(action.equalsIgnoreCase("Agregar")){
           String tipoDoc = request.getParameter("txtTipoDoc");
           String documento = request.getParameter("txtDoc");
           String nom = request.getParameter("txtNombre");
           String apellido = request.getParameter("txtApellido");
           String cel = request.getParameter("txtCelular");
           c.setApellido(apellido);
           c.setDocumento(Integer.parseInt(documento));
           c.setNombre(nom);
           c.setCelular(Integer.parseInt(cel));
           c.setTipodocumento(tipoDoc);
           
           dao.add(c);
           acceso=list;
                   
      }else if(action.equalsIgnoreCase("editar")){
          request.setAttribute("idcli",request.getParameter("id"));
          acceso=edit;
      }else if(action.equalsIgnoreCase("Actualizar")){
           int id = Integer.parseInt(request.getParameter("txtid"));
           String tipoDoc = request.getParameter("txtTipoDoc");
           String documento = request.getParameter("txtDoc");
           String nom = request.getParameter("txtNombre");
           String apellido = request.getParameter("txtApellido");
           String cel = request.getParameter("txtCelular");
           c.setId(id);
           c.setApellido(apellido);
           c.setDocumento(Integer.parseInt(documento));
           c.setNombre(nom);
           c.setCelular(Integer.parseInt(cel));
           c.setTipodocumento(tipoDoc);
           dao.edit(c);
           acceso=list;
      }else if(action.equalsIgnoreCase("eliminar")){
            int id = Integer.parseInt(request.getParameter("id"));
            c.setId(id);
            dao.delete(id);
            acceso=list;
      }
       RequestDispatcher vista = request.getRequestDispatcher(acceso);
       vista.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
