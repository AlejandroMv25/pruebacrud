
package Interfaces;

import Model.Cliente;
import java.util.List;

public interface CRUD {
    public List listar();
    public Cliente list(int id);
    public boolean add(Cliente cli);
    public boolean edit(Cliente cli);
    public boolean delete(int id);
}
